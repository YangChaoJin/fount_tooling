/*
*********************************************************************************************************
*	                                  
*	文件名称 : bsp_flash.c
*	版    本 : V1.0
*	说    明 : flash编程驱动
*	修改记录 :

*********************************************************************************************************
*/
#include "bsp_flash.h"

#define STM32_FLASH_BASE 0x08000000 	//STM32 FLASH的起始地址

#define STM32_FLASH_SIZE 128  //定义flash的大小

//#if defined STM32F10X_HD
//#define STM_PAGE_SIZE	2048
//#else
//#define STM_PAGE_SIZE	1024
//#endif
//u16 FLASH_BUF[STM_PAGE_SIZE/2];//最多是2K字节
/*
*********************************************************************************************************
*	函 数 名: FLASH_ReadHalfWord
*	形    参：addr : 要读数据的地址
*			  
*	返 回 值: 该地址的数值
*********************************************************************************************************
*/
u16 FLASH_ReadHalfWord(u32 addr)
{
	return *(vu16*)addr; 
}
/*
*********************************************************************************************************
*	函 数 名: FLASH_Write_NoCheck(u32 WriteAddr,u16 *pBuffer,u16 NumToWrite)   
*	形    参：WriteAddr : 要写数据的地址
*			  		pBuffer:pBuffer:数据指针
*						NumToWrite:半字节数
*	返 回 值: 无
*********************************************************************************************************
*/
void FLASH_Write_NoCheck(u32 WriteAddr,u16 *pBuffer,u16 NumToWrite)   
{ 			 		 
	u16 i;
	for(i=0;i<NumToWrite;i++)
	{
		FLASH_ProgramHalfWord(WriteAddr,pBuffer[i]);
	  WriteAddr+=2;//地址增加2.
	}  
} 
/*
*********************************************************************************************************
*	函 数 名: FLASH_Read(u32 ReadAddr,u16 *pBuffer,u16 NumToRead)     
*	形    参：ReadAddr : 要读数据的地址
*						NumToRead:半字节数
*	返 回 值: 无
*********************************************************************************************************
*/
void FLASH_Read(u32 ReadAddr,u16 *pBuffer,u16 NumToRead)   	
{
	u16 i;
	for(i=0;i<NumToRead;i++)
	{
		pBuffer[i]=FLASH_ReadHalfWord(ReadAddr);//读取2个字节.
		ReadAddr+=2;//偏移2个字节.	
	}
}
/*
*********************************************************************************************************
*	函 数 名: copy_app(u32 WriteAddr,u16 *pBuffer,u16 NumToWrite)   
*	形    参：WriteAddr : 要写数据的地址
*			  		pBuffer:pBuffer:数据指针
*						NumToWrite:半字节数
*	返 回 值: 无
*********************************************************************************************************
*/
void copy_app(u32 Writeaddr,u16 *pBuffer,u16 NumToWrite)
{
	int i=0;
	if(Writeaddr<STM32_FLASH_BASE||(Writeaddr>=(STM32_FLASH_BASE+1024*STM32_FLASH_SIZE)))return;//非法地址
	if(NumToWrite == 0 )return;//非法长度
	FLASH_Unlock();						//解锁
	FLASH_ErasePage(Writeaddr);//擦出当前页的数据
	for( i=0;i<NumToWrite;i++) //写所有的数据
	{
		FLASH_ProgramHalfWord(Writeaddr,pBuffer[i]);
		Writeaddr+=2;
	}
	FLASH_Lock();//上锁
}






